package ru.neoflex.meta.etl2.spark

import java.sql.{CallableStatement, Timestamp}
import java.util.Date

import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration
import org.apache.spark.rdd.{JdbcRDD, RDD, EmptyRDD}
import ru.neoflex.meta.etl2.ETLJobConst._
import ru.neoflex.meta.etl2.{ETLJobBase, JdbcETLContext, OracleSequence}
import scala.collection.{JavaConversions, immutable}
import ru.neoflex.meta.etl.functions._
import org.apache.spark.sql.functions._
import org.apache.livy.JobContext



class test4gitflowmergeJob extends ETLJobBase with org.apache.livy.Job[Unit] {

  
  var externalArgs = new Array[String](0);

  def setParams(args: Array[String]) = {
    externalArgs = args;
  }
  	
  @throws(classOf[Exception])
  override def call(jobContext: JobContext): Unit = {
    if( externalArgs.length == 0) {
      throw new RuntimeException("Job params is not set")
    }
    sc = jobContext.sc();
    parse(externalArgs.toSeq);
    try{
      val sparkSession = SparkSession.builder.config(sc.getConf).getOrCreate()
      runJob(sparkSession)
    }finally {

    }
  }

  override def getApplicationName: String = {
    "test4gitflowmerge"
  }
  
  def run(spark: SparkSession): Any = {
  	
    	val Local_0 = getLocal_0(spark)


    	Local_1(spark, Local_0)
 
    
  }
  def getLocal_0(spark: SparkSession) = {
  	import spark.implicits._
    import scala.reflect.runtime.universe._
    val fileName = s""""""
    val paths = fileName.split(",")
    
    spark
    .read 
    .schema(newProductEncoder(typeTag[Local_0Schema]).schema)
    .format("json")
    .load(paths: _*) 		
    .as[Local_0Schema]
 
  }

  def Local_1(spark: SparkSession, ds: Dataset[Local_0Schema]): Unit = {        
    val fileName = s""""""
    logger.logInfo(s"LocalTarget Local_1 fileName: ${fileName}")   
    val dsOut = ds    
    dsOut
        .write
        .mode(SaveMode.Append)
        .format("json")
        .save(fileName)    
    
 
  }

 

org.apache.spark.sql.catalyst.encoders.OuterScopes.addOuterScope(this)
case class Local_0Schema(
) extends Serializable

}


object test4gitflowmergeJob {
   def main(args: Array[String]): Unit = {
     new test4gitflowmergeJob().sparkMain(args)
  }
}

